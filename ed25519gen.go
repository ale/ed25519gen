package main

import (
	"flag"
	"io/ioutil"
	"log"

	"crypto/ed25519"
)

var (
	outPubKey  = flag.String("output", "public.key", "public key output `path`")
	outPrivKey = flag.String("outkey", "secret.key", "private key output `path`")
)

func main() {
	log.SetFlags(0)
	flag.Parse()

	pub, priv, err := ed25519.GenerateKey(nil)
	if err != nil {
		log.Fatal(err)
	}

	if err := ioutil.WriteFile(*outPubKey, pub, 0644); err != nil {
		log.Fatal(err)
	}
	if err := ioutil.WriteFile(*outPrivKey, priv, 0600); err != nil {
		log.Fatal(err)
	}
}
